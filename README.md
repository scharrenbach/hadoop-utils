The DDIS Hadoop Utils Package
==============


This software comes with no warranty.

Licensing
--------------
Copyright 2013 University of Zurich, Department of Informatics

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Acknowledgements
--------------

The research leading to these results has received funding 
from the European Union Seventh Framework Programme FP7/2007-2011 
under grant agreement n° 296126.


Dependencies
--------------

- hadoop2
- mrunit
- jdom2
- Testng
