package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.hadoop.conf.Configuration;

/**
 * <p>
 * Utility functions.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class Utils {

	/**
	 * <p>
	 * Extracts a value from a hadoop {@link Configuration}. In case the value
	 * in the {@link Configuration} is <code>null</code> and the null value
	 * permitted flag is true this methods throws a {@link NullPointerException}
	 * . In case the value is <code>null</code> but the flag permits
	 * <code>null</code> values, then this method returns <code>null</code>.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.0.1
	 * @since 0.0.1
	 * @param conf
	 * @param key
	 * @param nullValuePermitted
	 * @return
	 */
	public static String getValue(Configuration conf, String key,
			boolean nullValuePermitted) {
		final String value = conf.get(key);
		if (!nullValuePermitted && value == null) {
			throw new NullPointerException(String.format(
					"Value for key %s must not be null!", key));
		}
		return value;
	}

	/**
	 * <p>
	 * Extracts a value from a hadoop {@link Configuration}. In case the value
	 * in the {@link Configuration} is null, the specified default value will be
	 * taken. The default value can be <code>null</code>.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.0.1
	 * @since 0.0.1
	 * @param conf
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static String getValue(Configuration conf, String key,
			String defaultValue) {
		final String value = conf.get(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

}
