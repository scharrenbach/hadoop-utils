package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Reads a CSV file and outputs it as a {@link MapWritable}.
 * </p>
 * 
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.2
 * @since 0.0.2
 * 
 */
class CsvInputFormat extends FileInputFormat<LongWritable, MapWritable> {

	private static final Logger _log = LoggerFactory
			.getLogger(CsvInputFormat.class);

	//
	//
	//

	public static final String ENCLOSING_CHAR = "csvinput.enclosing-char";
	public static final String SEPARATOR_CHAR = "csvinput.separator-char";
	public static final String ENCODING_KEY = "csvinput.encoding";
	public static final String DEFAULT_ENCODING = "UTF-8";
	public static final String DEFAULT_SEPARATOR_CHAR = Character.toString(',');

	/**
	 * <p>
	 * Factory method for creating a new {@link CsvRecordReader}.
	 * </p>
	 */
	@Override
	public RecordReader<LongWritable, MapWritable> createRecordReader(
			InputSplit is, TaskAttemptContext tac) {
		return new CsvRecordReader();
	}

	/**
	 * <p>
	 * The {@link CsvRecordReader} parses the {@link InputSplit} according to
	 * the csv standard and creates a map for each csv record.
	 * </p>
	 * <p>
	 * Possible parameters can be configured setting the following properties:
	 * <table border="1">
	 * <tr>
	 * <th>Property key</th>
	 * <th>Property value</th>
	 * <th>Default value</th>
	 * </tr>
	 * <tr>
	 * <td>csvinput.separator-char</td>
	 * <td>Character that separates fields.</td>
	 * <td>','</td>
	 * </tr>
	 * <tr>
	 * <td>csvinput.enclosing-char</td>
	 * <td>Character that encloses fields.</td>
	 * <td>null</td>
	 * </tr>
	 * <tr>
	 * <td>csvinput.encoding</td>
	 * <td>Encoding of input</td>
	 * <td>UTF-8</td>
	 * </tr>
	 * </table>
	 * </p>
	 * <p>
	 * Currently, the keys to these records are the column numbers, starting at
	 * zero. No checks regarding the number of columns over all rows is
	 * enforced.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.0.2
	 * @since 0.0.2
	 * 
	 */
	public static class CsvRecordReader extends
			RecordReader<LongWritable, MapWritable> {
		private byte[] _enclosing;
		private long _startPos;
		private long _endPos;
		private FSDataInputStream _splitIn;
		private LongWritable _key = new LongWritable();
		private MapWritable _value = new MapWritable();
		private byte[] _fieldSeparator;
		private byte[] _lineSeparator;

		/**
		 * <p>
		 * Extracts the field separator and the enclosing characters, as well as
		 * the encoding from the {@link TaskAttemptContext}.
		 * </p>
		 */
		@Override
		public void initialize(InputSplit split, TaskAttemptContext context)
				throws IOException, InterruptedException {
			final Configuration conf = context.getConfiguration();

			// Extract values from config.
			final String encoding = Utils.getValue(conf, ENCODING_KEY,
					DEFAULT_ENCODING);

			// TODO implement check that values are valid characters under the
			// encoding.
			final String enclosingTmp = Utils.getValue(conf, ENCLOSING_CHAR,
					true);
			_enclosing = (enclosingTmp == null || enclosingTmp.isEmpty()) ? null
					: enclosingTmp.getBytes(encoding);

			final String fieldSeparatorTmp = Utils.getValue(conf,
					SEPARATOR_CHAR, DEFAULT_SEPARATOR_CHAR);
			_fieldSeparator = (fieldSeparatorTmp == null || fieldSeparatorTmp
					.isEmpty()) ? null : fieldSeparatorTmp.getBytes(encoding);

			// TODO make this configurable.
			_lineSeparator = "\n".getBytes();

			_log.info("Created new CsvInputFormat "
					+ "with separator '{}' and enclosed by '{}'", new String(
					_fieldSeparator), _enclosing == null ? null : new String(
					_enclosing));

			// We assume that we read from files only.
			final FileSplit fileSplit = (FileSplit) split;

			// Determine position in the file
			_startPos = fileSplit.getStart();
			_endPos = _startPos + fileSplit.getLength();

			// Open the file in the file system and go to the starting position.
			final Path csvFile = fileSplit.getPath();

			final FileSystem fs = csvFile.getFileSystem(context
					.getConfiguration());
			_splitIn = fs.open(fileSplit.getPath());
			_splitIn.seek(_startPos);

			_key = new LongWritable();
			_value = new MapWritable();
		}

		/**
		 * <p>
		 * Reads input until a new record is found, an <code>EOF</code> was read
		 * or the {@link FileSplit} has no more input.
		 * </p>
		 * 
		 * @return <code>true</code>, if a new recored was found,
		 *         <code>false</code> else.
		 */
		@Override
		public boolean nextKeyValue() throws IOException, InterruptedException {
			int column = 0;

			DataOutputBuffer buffer = new DataOutputBuffer();

			// indices for checking a match of an field separator, a newline or
			// an enclosing.
			final int[] idx = new int[3];

			// index for matching field separator
			final int idxMatchFieldSep = 0;

			// index for matching enclosing
			final int idxMatchEncl = 1;

			// index for matching line separator
			final int idxMatchLineSep = 2;

			// flag whether we are reading a value or not.
			boolean inValue = _enclosing == null;

			// flag whether we are starting a new field or not.
			boolean newField = false;

			// flag whether we are starting a new line or not.
			boolean newLine = false;

			boolean hasMoreInput = true;

			_key.set(_splitIn.getPos());

			try {
				while (hasMoreInput) {
					int b = _splitIn.read();
					// end of file:
					if (b == -1) {
						_log.debug("No match, EOF.");
						hasMoreInput = false;
						break;
					}

					// see if we've passed the stop point:
					if (_splitIn.getPos() >= _endPos) {
						_log.debug("No match, passed stop point!");
						hasMoreInput = false;
						break;
					}

					// If we are not processing a new value we can have
					if (!inValue) {
						// 1) a new line
						newLine = checkMatch(b, _lineSeparator, idx,
								idxMatchLineSep);
						newField = checkMatch(b, _fieldSeparator, idx,
								idxMatchFieldSep);
						// 2) an enclosing char -- if defined
						if (_enclosing != null) {
							inValue = checkMatch(b, _enclosing, idx,
									idxMatchEncl);
						}
						// 3) a field separator
						else {
							inValue = newField;
						}
					}
					// In case we are processing a value ...
					else {
						// if the enclosing is defined we have to look for a
						// matching char.
						if (_enclosing != null) {
							inValue = !checkMatch(b, _enclosing, idx,
									idxMatchEncl);
						}
						// in case no enclosing is defined, we have to check
						// whether
						// we find a field separator or a newline.
						else {
							newLine = checkMatch(b, _lineSeparator, idx,
									idxMatchLineSep);
							newField = checkMatch(b, _fieldSeparator, idx,
									idxMatchFieldSep);
							inValue = !(newLine || newField);
						}
						// if we are still processing a value, then we write it
						// to
						// the buffer
						if (inValue) {
							buffer.write(b);
						}
						// Otherwise, we add a new value to the result and reset
						// the
						// buffer.
						else {
							final Text entryKey = new Text(
									Integer.toString(column));
							final Text entryVal = new Text(buffer.getData());
							++column;
							_log.debug(
									"Adding new entry to map: \"{}\":\"{}\"",
									entryKey, entryVal);
							_value.put(entryKey, entryVal);
							newField = false;
							inValue = _enclosing == null;
							buffer.close();
							buffer.reset();
						}
					}
					hasMoreInput = !newLine;
				}
			}
			// Always close the buffer.
			finally {
				buffer.close();
			}

			// If we found a new line, we found a new record
			if (newLine) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * <p>
		 * Checks for matches.
		 * </p>
		 * 
		 * @param b
		 *            the byte to match.
		 * @param match
		 *            the byte sequence to match.
		 * @param idx
		 *            list of concurring indices for matching.
		 * @param i
		 *            the index to use from <code>idx</code>.
		 * @return
		 */
		private boolean checkMatch(int b, byte[] match, int[] idx, final int i) {
			// check if we're matching the field separator
			if (b == match[idx[i]]) {
				++idx[i];
				if (idx[i] >= match.length) {
					_log.debug("Found match for '{}'", new String(match));
					// reset all inidices in case of a match
					for (int n = 0; n < idx.length; ++n) {
						idx[n] = 0;
					}
					return true;
				}
			}
			// in case this very index has no match, reset only this very index.
			else {
				idx[i] = 0;
			}
			return false;
		}

		@Override
		public LongWritable getCurrentKey() throws IOException,
				InterruptedException {
			return _key;
		}

		@Override
		public MapWritable getCurrentValue() throws IOException,
				InterruptedException {
			return _value;

		}

		/**
		 * <p>
		 * Return the ratio between the start and the end position within the
		 * file.
		 * </p>
		 */
		@Override
		public float getProgress() throws IOException, InterruptedException {
			return (_splitIn.getPos() - _startPos)
					/ (float) (_endPos - _startPos);
		}

		/**
		 * <p>
		 * Close the file system.
		 * </p>
		 */
		@Override
		public void close() throws IOException {
			_splitIn.close();
		}

	}

}
