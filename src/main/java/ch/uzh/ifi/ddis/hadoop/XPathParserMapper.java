package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * <p>
 * The {@link XPathParserMapper} maps the content of XML-tags to keys.
 * </p>
 * <p>
 * Currently, the value mapped is te test content of those nodes matching the
 * specified xpath expression. The key is currently a {@link NullWritable}.
 * </p>
 * <p>
 * The xpath expression can be set via the property
 * <code>xpath-expression</code>.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 */
public class XPathParserMapper extends
		Mapper<LongWritable, Text, NullWritable, Text> {
	private static final Logger _log = LoggerFactory
			.getLogger(XPathParserMapper.class);

	public static final String KEY_XPATH_EXPRESSION = "xpath-expression";

	//
	//
	//

	private String _xpathExpression;

	public XPathParserMapper() {
		_log.debug("Created {}", this.getClass());
		return;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		super.setup(context);
		_xpathExpression = Utils.getValue(context.getConfiguration(),
				KEY_XPATH_EXPRESSION, false);
	}

	/**
	 * <p>
	 * Apply the xpath expression and insert all text values for the
	 * corresponding tags as a value.
	 * </p>
	 */
	@Override
	public void map(LongWritable srcKey, Text srcValue, Context context)

	throws IOException, InterruptedException {
		_log.debug("Started map for {}", this.getClass());

		final String xmlString = srcValue.toString();

		try {
			final DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document doc = builder.parse(new ByteArrayInputStream(
					xmlString.getBytes()));
			final XPathFactory xPathfactory = XPathFactory.newInstance();
			final XPath xpath = xPathfactory.newXPath();
			final XPathExpression expr = xpath.compile(_xpathExpression);
			final NodeList nl = (NodeList) expr.evaluate(doc,
					XPathConstants.NODESET);
			final int maxI = nl.getLength();
			for (int i = 0; i < maxI; ++i) {
				final Node item = nl.item(i);
				context.write(NullWritable.get(),
						new Text(item.getTextContent()));
			}
		} catch (Exception e) {
			_log.error("", e);
		}
		_log.debug("Finished map for {}", this.getClass());

	}

}