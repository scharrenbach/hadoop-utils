package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * The {@link PrintMapper} maps the content of XML-tags to keys.
 * </p>
 * <p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 */
public class PrintMapper extends Mapper<Object, Object, Object, Object> {
	private static final Logger _log = LoggerFactory
			.getLogger(PrintMapper.class);

	private OutputStream _out;

	//
	//
	//

	public PrintMapper() {
		_log.debug("Created new {}", PrintMapper.class);
		return;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		super.setup(context);
		// TODO make this configurable
		_out = System.out;
	}

	/**
	 * <p>
	 * </p>
	 */
	@Override
	public void map(Object key, Object value, Context context)

	throws IOException, InterruptedException {
		_log.debug("Started map for {}", PrintMapper.class);
		try {
			_out.write(String.format("{\"%s\":\"%s\"}\n", key, value)
					.getBytes());
			context.write(key, value);
		} catch (Exception e) {
			_log.error("", e);
		}
		_log.debug("Finished map for {}", PrintMapper.class);

	}

}