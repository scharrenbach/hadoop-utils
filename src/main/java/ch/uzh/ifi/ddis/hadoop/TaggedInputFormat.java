package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

/**
 * <p>
 * Reads records that are delimited by a specific begin/end tag from files. Keys
 * are the start position of the tag (including the tag) in the file and values
 * are everything within the tags, including start and end tag.
 * </p>
 * <p>
 * This input format allows reading XML files with no nested tags of the same
 * type into Hadoop.
 * </p>
 * <p>
 * Taken from this <a href="http://xmlandhadoop.blogspot.ch/">website</a>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class TaggedInputFormat extends TextInputFormat {

	public static final String START_TAG_KEY = "taginput.start";
	public static final String END_TAG_KEY = "taginput.end";
	public static final String ENCODING_KEY = "taginput.encoding";
	public static final String DEFAULT_ENCODING = "UTF-8";

	/**
	 * <p>
	 * </p>
	 */
	@Override
	public RecordReader<LongWritable, Text> createRecordReader(InputSplit is,
			TaskAttemptContext tac) {

		return new XmlRecordReader();

	}

	private static String getValue(Configuration conf, String key,
			boolean nullValuePermitted) {
		final String value = conf.get(key);
		if (!nullValuePermitted && value == null) {
			throw new NullPointerException(String.format(
					"Value for key %s must not be null!", key));
		}
		return value;
	}

	public static class XmlRecordReader extends
			RecordReader<LongWritable, Text> {
		private byte[] _startTag;
		private byte[] _endTag;
		private long _startPos;
		private long _endPos;
		private FSDataInputStream _fsin;
		private DataOutputBuffer _buffer = new DataOutputBuffer();
		private LongWritable _key = new LongWritable();
		private Text _value = new Text();

		/**
		 * 
		 */
		@Override
		public void initialize(InputSplit is, TaskAttemptContext tac)
				throws IOException, InterruptedException {
			final Configuration conf = tac.getConfiguration();

			// Extract values from config.
			String encoding = getValue(conf, ENCODING_KEY, true);
			if (encoding == null) {
				encoding = DEFAULT_ENCODING;
			}
			_startTag = getValue(conf, START_TAG_KEY, false).getBytes(encoding);
			_endTag = getValue(conf, END_TAG_KEY, false).getBytes(encoding);

			// We assume that we read from files only.
			final FileSplit fileSplit = (FileSplit) is;

			// Determine position in the file
			_startPos = fileSplit.getStart();
			_endPos = _startPos + fileSplit.getLength();

			// Open the file in the file system and go to the starting position.
			final Path xmlFile = fileSplit.getPath();

			final FileSystem fs = xmlFile.getFileSystem(tac.getConfiguration());
			_fsin = fs.open(fileSplit.getPath());
			_fsin.seek(_startPos);
		}

		/**
		 * 
		 */
		@Override
		public boolean nextKeyValue() throws IOException, InterruptedException {
			if (_fsin.getPos() < _endPos) {
				if (readUntilMatch(_startTag, false)) {
					try {
						_buffer.write(_startTag);
						if (readUntilMatch(_endTag, true)) {

							_value.set(_buffer.getData(), 0,
									_buffer.getLength());
							_key.set(_fsin.getPos());
							return true;
						}
					} finally {
						_buffer.reset();
					}
				}
			}
			return false;
		}

		@Override
		public LongWritable getCurrentKey() throws IOException,
				InterruptedException {
			return _key;
		}

		@Override
		public Text getCurrentValue() throws IOException, InterruptedException {
			return _value;

		}

		/**
		 * <p>
		 * Return the ratio between the start and the end position within the
		 * file.
		 * </p>
		 */
		@Override
		public float getProgress() throws IOException, InterruptedException {
			return (_fsin.getPos() - _startPos) / (float) (_endPos - _startPos);
		}

		/**
		 * <p>
		 * Close the file system.
		 * </p>
		 */
		@Override
		public void close() throws IOException {
			_fsin.close();
		}

		private boolean readUntilMatch(byte[] match, boolean withinBlock)
				throws IOException {
			int i = 0;
			while (true) {
				int b = _fsin.read();
				// end of file:
				if (b == -1)
					return false;
				// save to buffer:
				if (withinBlock)
					_buffer.write(b);

				// check if we're matching:
				if (b == match[i]) {
					i++;
					if (i >= match.length)
						return true;
				} else
					i = 0;
				// see if we've passed the stop point:
				if (!withinBlock && i == 0 && _fsin.getPos() >= _endPos)
					return false;
			}
		}

	}

}
