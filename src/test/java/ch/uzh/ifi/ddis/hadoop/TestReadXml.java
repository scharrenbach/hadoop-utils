package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.ClusterMapReduceTestCase;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 * <p>
 * Tests reading input from XML files using {@link TaggedInputFormat}.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class TestReadXml extends ClusterMapReduceTestCase {

	private static final Logger _log = LoggerFactory
			.getLogger(TestReadXml.class);

	@Test
	public void runTest() {
		try {
			String input = "./src/test/resources/test.xml";
			String output = "./test-output/hadoop";

			// final JobConf conf = createJobConf();
			final Configuration conf = new Configuration();

			conf.set(TaggedInputFormat.START_TAG_KEY, "<P>");
			conf.set(TaggedInputFormat.END_TAG_KEY, "</P>");
			conf.set(XPathParserMapper.KEY_XPATH_EXPRESSION, "//P");
			conf.set(
					"io.serializations",
					"org.apache.hadoop.io.serializer.JavaSerialization,"
							+ "org.apache.hadoop.io.serializer.WritableSerialization");

			_log.info("Started creating Hadoop job...");
			final Job job = Job.getInstance(conf, "jobName");

			FileInputFormat.setInputPaths(job, input);
			// job.setJarByClass(TestXmlnputFormatJob.class);
			job.setMapperClass(XPathParserMapper.class);

			job.setReducerClass(PrintReducer.class);
			// job.setNumReduceTasks(1);

			job.setInputFormatClass(TaggedInputFormat.class);
			job.setOutputKeyClass(NullWritable.class);
			job.setOutputValueClass(Text.class);
			_log.info("Finished creating Hadoop job...");

			_log.info("Started creating local HDFS...");
			final Path outPath = new Path(output);
			FileOutputFormat.setOutputPath(job, outPath);
			FileSystem dfs = FileSystem.get(outPath.toUri(), conf);
			if (dfs.exists(outPath)) {
				dfs.delete(outPath, true);
			}
			_log.info("Finished creating local HDFS...");

			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	}

	/**
	 * <p>
	 * Simple setup. Calls super{@link #setUp()} as last call.
	 * </p>
	 */
	@Override
	protected void setUp() throws Exception {

		System.setProperty("hadoop.log.dir", "/tmp/logs");
		super.setUp();
	}

}
