package ch.uzh.ifi.ddis.hadoop;

/*
 * #%L
 * DDIS Hadoop Utils
 * %%
 * Copyright (C) 2014 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.ClusterMapReduceTestCase;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * <p>
 * Tests reading input from XML files using {@link CsvInputFormat}.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class TestReadCsv extends ClusterMapReduceTestCase {

	private static final Logger _log = LoggerFactory
			.getLogger(TestReadCsv.class);

	/**
	 * <p>
	 * Convenience method for keeping compatibility with JUnit.
	 * </p>
	 */
	@org.junit.Test
	public void test() {
		String input = "./src/test/resources/test_with_quotes.csv";
		String output = "./test-output/hadoop";
		String enclosingChar = Character.toString('\"');
		String separatorChar = Character.toString(' ');
		runTest(input, output, separatorChar, enclosingChar);
	}

	@Test
	@Parameters({ "input", "output", "separator", "enclosing" })
	public void runTest(String input, String output,
			@Optional(value = "") String separatorChar,
			@Optional(value = "") String enclosingChar) {
		try {
			// final JobConf conf = createJobConf();
			final Configuration conf = new Configuration();

			conf.set(CsvInputFormat.ENCLOSING_CHAR, separatorChar);
			conf.set(CsvInputFormat.SEPARATOR_CHAR, enclosingChar);
			conf.set(
					"io.serializations",
					"org.apache.hadoop.io.serializer.JavaSerialization,"
							+ "org.apache.hadoop.io.serializer.WritableSerialization");

			_log.info("Started creating Hadoop job...");
			final Job job = Job.getInstance(conf, "jobName");

			FileInputFormat.setInputPaths(job, input);
			// job.setJarByClass(TestXmlnputFormatJob.class);
			job.setMapperClass(PrintMapper.class);

			job.setNumReduceTasks(0);

			job.setInputFormatClass(CsvInputFormat.class);
			job.setOutputKeyClass(LongWritable.class);
			job.setOutputValueClass(Text.class);
			_log.info("Finished creating Hadoop job...");

			_log.info("Started creating local HDFS...");
			final Path outPath = new Path(output);
			FileOutputFormat.setOutputPath(job, outPath);
			FileSystem dfs = FileSystem.get(outPath.toUri(), conf);
			if (dfs.exists(outPath)) {
				dfs.delete(outPath, true);
			}
			_log.info("Finished creating local HDFS...");

			job.waitForCompletion(true);
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	}

	/**
	 * <p>
	 * Simple setup. Calls super{@link #setUp()} as last call.
	 * </p>
	 */
	@Override
	@BeforeTest
	protected void setUp() throws Exception {

		System.setProperty("hadoop.log.dir", "/tmp/logs");
		super.setUp();
	}

	@Override
	@AfterTest
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
